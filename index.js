// Direction:
// Create two event when a user types in the first & last Names inputs
// When this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

// Stretch goal: Instead of an anonymous function, create a new function that the two event listers will call


//Guide question: Where do the names come from and where should they go?


let input1 = document.querySelector('#txt-first-name');
let input2 = document.querySelector('#txt-last-name');
let spanFullName = document.querySelector('#span-full-name');


let spanfullName = (e) => {

	spanFullName.textContent = `${input1.value} ${input2.value}`;

};

input1.addEventListener(`keyup`, spanfullName);
input2.addEventListener(`keyup`, spanfullName);


/* Other Solution by the Instructor

let firstName = document.querySelector('#txt-first-name');
let lastName = document.querySelector('#txt-last-name');
let spanFullName = document.querySelector('#span-full-name');

const updateFullName = () => {
	
	let firstName = firstName.value;
	let lastName =   firstName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`

}

firstName.addEventListener(`keyup`, () => {

	updateFullName();

});

lastName.addEventListener(`keyup`, updateFullName);

*/